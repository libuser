# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Red Hat, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Miloslav Trmač <mitr@volny.cz>, 2011
# Nikola Pajtic <salgeras@gmail.com>, 2008
msgid ""
msgstr ""
"Project-Id-Version: libuser 0.60\n"
"Report-Msgid-Bugs-To: http://bugzilla.redhat.com/bugzilla/\n"
"POT-Creation-Date: 2023-03-14 15:37+0100\n"
"PO-Revision-Date: 2013-04-29 04:37-0400\n"
"Last-Translator: Miloslav Trmač <mitr@volny.cz>\n"
"Language-Team: Serbian (Latin) (http://www.transifex.com/projects/p/fedora/"
"language/sr@latin/)\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Zanata 3.6.2\n"

#: apps/apputil.c:195 apps/apputil.c:199
#, c-format
msgid "Failed to drop privileges.\n"
msgstr "Neuspelo odbacivanje povlastica.\n"

#: apps/apputil.c:208
#, c-format
msgid "Internal error.\n"
msgstr "Unutrašnja greška.\n"

#: apps/apputil.c:234
#, c-format
msgid "%s is not authorized to change the finger info of %s\n"
msgstr "%s nije ovlašćen da menja finger podatke za %s\n"

#: apps/apputil.c:236
msgid "Unknown user context"
msgstr "Nepoznat korisnički kontekst"

#: apps/apputil.c:245
#, c-format
msgid "Can't set default context for /etc/passwd\n"
msgstr "Ne mogu da postavim podrazumevani kontekst za /etc/passwd\n"

#: apps/apputil.c:254
#, c-format
msgid "Error initializing PAM.\n"
msgstr "Greška pri inicijalizaciji PAM-a.\n"

#: apps/apputil.c:264 apps/apputil.c:291
#, c-format
msgid "Authentication failed for %s.\n"
msgstr "Neuspela autentifikacija za %s.\n"

#: apps/apputil.c:272
#, c-format
msgid "Internal PAM error `%s'.\n"
msgstr "Unutrašnja PAM greška „%s“.\n"

#: apps/apputil.c:277
#, c-format
msgid "Unknown user authenticated.\n"
msgstr "Autentifikovan nepoznat korisnik.\n"

#: apps/apputil.c:281
#, c-format
msgid "User mismatch.\n"
msgstr "Neslaganje korisnika.\n"

#: apps/lchage.c:86 apps/lchfn.c:54 apps/lchsh.c:46 apps/lgroupadd.c:48
#: apps/lgroupdel.c:44 apps/lgroupmod.c:56 apps/lid.c:116 apps/lnewusers.c:47
#: apps/lpasswd.c:48 apps/luseradd.c:57 apps/luserdel.c:47 apps/lusermod.c:57
msgid "prompt for all information"
msgstr ""

#: apps/lchage.c:88
msgid "list aging parameters for the user"
msgstr ""

#: apps/lchage.c:90
msgid "minimum days between password changes"
msgstr ""

#: apps/lchage.c:90 apps/lchage.c:92 apps/lchage.c:95 apps/lchage.c:98
#: apps/lchage.c:101 apps/lchage.c:104
msgid "DAYS"
msgstr ""

#: apps/lchage.c:92
msgid "maximum days between password changes"
msgstr ""

#: apps/lchage.c:94
msgid "date of last password change in days since 1/1/70"
msgstr ""

#: apps/lchage.c:97
msgid ""
"number of days after password expiration date when account is considered "
"inactive"
msgstr ""

#: apps/lchage.c:100
msgid "password expiration date in days since 1/1/70"
msgstr ""

#: apps/lchage.c:103
msgid "days before expiration to begin warning user"
msgstr ""

#: apps/lchage.c:116 apps/lid.c:131 apps/lpasswd.c:69 apps/luseradd.c:102
#: apps/luserdel.c:61 apps/lusermod.c:102
msgid "[OPTION...] user"
msgstr "[OPCIJA...] korisnik"

#: apps/lchage.c:119 apps/lchfn.c:72 apps/lchsh.c:60 apps/lgroupadd.c:66
#: apps/lgroupdel.c:58 apps/lgroupmod.c:87 apps/lid.c:134 apps/lnewusers.c:68
#: apps/lpasswd.c:72 apps/luseradd.c:105 apps/luserdel.c:64 apps/lusermod.c:105
#, c-format
msgid "Error parsing arguments: %s.\n"
msgstr "Greška pri tumačenju argumenata: %s.\n"

#: apps/lchage.c:129 apps/lpasswd.c:87 apps/luseradd.c:121 apps/luserdel.c:73
#: apps/lusermod.c:117
#, c-format
msgid "No user name specified.\n"
msgstr "Nije navedeno korisničko ime.\n"

#: apps/lchage.c:140 apps/lchfn.c:106 apps/lchsh.c:93 apps/lgroupadd.c:104
#: apps/lgroupdel.c:79 apps/lgroupmod.c:122 apps/lid.c:181 apps/lnewusers.c:80
#: apps/lpasswd.c:98 apps/luseradd.c:146 apps/luserdel.c:83 apps/lusermod.c:160
#: samples/enum.c:56 samples/testuser.c:71
#, c-format
msgid "Error initializing %s: %s.\n"
msgstr "Greška pri inicijalizaciji %s: %s.\n"

#: apps/lchage.c:150 apps/lchfn.c:118 apps/lchsh.c:105 apps/lpasswd.c:151
#: apps/luserdel.c:92 apps/lusermod.c:176
#, c-format
msgid "User %s does not exist.\n"
msgstr "Korisnik %s ne postoji.\n"

#: apps/lchage.c:164
#, c-format
msgid "Account is locked.\n"
msgstr "Nalog je zaključan.\n"

#: apps/lchage.c:166
#, c-format
msgid "Account is not locked.\n"
msgstr "Nalog nije zaključan.\n"

#: apps/lchage.c:170
#, c-format
msgid "Minimum:\t%ld\n"
msgstr "Najmanji:\t%ld\n"

#: apps/lchage.c:174
#, c-format
msgid "Maximum:\t%ld\n"
msgstr "Najveći:\t%ld\n"

#: apps/lchage.c:176
#, c-format
msgid "Maximum:\tNone\n"
msgstr ""

#: apps/lchage.c:179
#, c-format
msgid "Warning:\t%ld\n"
msgstr "Upozorenje:\t%ld\n"

#: apps/lchage.c:184
#, c-format
msgid "Inactive:\t%ld\n"
msgstr "Neaktivno:\t%ld\n"

#: apps/lchage.c:186
#, c-format
msgid "Inactive:\tNever\n"
msgstr ""

#: apps/lchage.c:190 apps/lchage.c:198 apps/lchage.c:208
msgid "Must change password on next login"
msgstr ""

#: apps/lchage.c:192 apps/lchage.c:200 apps/lchage.c:210 apps/lchage.c:219
msgid "Never"
msgstr "Nikada"

#: apps/lchage.c:195
#, c-format
msgid "Last Change:\t%s\n"
msgstr "Poslednja promena:\t%s\n"

#: apps/lchage.c:205
#, c-format
msgid "Password Expires:\t%s\n"
msgstr "Lozinka ističe:\t%s\n"

#: apps/lchage.c:217
#, c-format
msgid "Password Inactive:\t%s\n"
msgstr "Lozinka neaktivna:\t%s\n"

#: apps/lchage.c:223
#, c-format
msgid "Account Expires:\t%s\n"
msgstr "Nalog ističe:\t%s\n"

#: apps/lchage.c:244
#, c-format
msgid "Failed to modify aging information for %s: %s\n"
msgstr "Neuspela izmena zastarelih podataka za %s: %s\n"

#: apps/lchfn.c:69 apps/lchsh.c:57
msgid "[OPTION...] [user]"
msgstr "[OPCIJA...] [korisnik]"

#: apps/lchfn.c:89 apps/lchsh.c:77 apps/lid.c:167
#, c-format
msgid "No user name specified, no name for uid %d.\n"
msgstr "Nije navedeno korisničko ime, nema imena za uid %d.\n"

#: apps/lchfn.c:99
#, c-format
msgid "Changing finger information for %s.\n"
msgstr "Menjam finger podatke za %s.\n"

#: apps/lchfn.c:141
msgid "Full Name"
msgstr "Puno ime"

#: apps/lchfn.c:151
msgid "Surname"
msgstr "Prezime"

#: apps/lchfn.c:162
msgid "Given Name"
msgstr "Kršteno ime"

#: apps/lchfn.c:172
msgid "Office"
msgstr "Kancelarija"

#: apps/lchfn.c:181
msgid "Office Phone"
msgstr "Kancelarijski telefon"

#: apps/lchfn.c:190
msgid "Home Phone"
msgstr "Kućni telefon"

#: apps/lchfn.c:200
msgid "E-Mail Address"
msgstr "Adresa e-pošte"

#: apps/lchfn.c:213
#, c-format
msgid "Finger information not changed:  input error.\n"
msgstr "Finger podaci nisu promenjeni:  greška u unosu.\n"

#: apps/lchfn.c:276
msgid "Finger information changed.\n"
msgstr "Finger podaci su promenjeni.\n"

#: apps/lchfn.c:279
#, c-format
msgid "Finger information not changed: %s.\n"
msgstr "Finger podaci nisu promenjeni: %s.\n"

#: apps/lchsh.c:86
#, c-format
msgid "Changing shell for %s.\n"
msgstr "Menjam ljusku za %s.\n"

#: apps/lchsh.c:118
msgid "New Shell"
msgstr "Nova ljuska"

#: apps/lchsh.c:125 apps/lchsh.c:145
#, c-format
msgid "Shell not changed: %s\n"
msgstr "Ljuska nije promenjena: %s\n"

#: apps/lchsh.c:140
msgid "Shell changed.\n"
msgstr "Ljuska je promenjena.\n"

#: apps/lgroupadd.c:50
msgid "gid for new group"
msgstr ""

#: apps/lgroupadd.c:50 apps/lgroupmod.c:58 apps/lpasswd.c:57 apps/lpasswd.c:60
#: apps/luseradd.c:69 apps/lusermod.c:67 apps/lusermod.c:69
msgid "NUM"
msgstr ""

#: apps/lgroupadd.c:52
msgid "create a system group"
msgstr ""

#: apps/lgroupadd.c:63 apps/lgroupdel.c:55 apps/lgroupmod.c:84
msgid "[OPTION...] group"
msgstr "[OPCIJA...] grupa"

#: apps/lgroupadd.c:76 apps/lgroupdel.c:68 apps/lgroupmod.c:96
#, c-format
msgid "No group name specified.\n"
msgstr "Nije navedeno ime grupe.\n"

#: apps/lgroupadd.c:90 apps/lgroupmod.c:109 apps/lnewusers.c:175
#: apps/luseradd.c:168 apps/lusermod.c:130
#, c-format
msgid "Invalid group ID %s\n"
msgstr "Neispravan ID grupe %s\n"

#: apps/lgroupadd.c:122
#, c-format
msgid "Group creation failed: %s\n"
msgstr "Neuspelo pravljenje grupe: %s\n"

#: apps/lgroupdel.c:88 apps/lgroupmod.c:137 apps/lpasswd.c:157
#, c-format
msgid "Group %s does not exist.\n"
msgstr "Grupa %s ne postoji.\n"

#: apps/lgroupdel.c:95
#, c-format
msgid "Group %s could not be deleted: %s\n"
msgstr "Grupa %s ne može biti izbrisana: %s\n"

#: apps/lgroupmod.c:58
msgid "set GID for group"
msgstr ""

#: apps/lgroupmod.c:60
msgid "change group to have given name"
msgstr ""

#: apps/lgroupmod.c:60
msgid "NAME"
msgstr ""

#: apps/lgroupmod.c:62 apps/luseradd.c:77
msgid "plaintext password for use with group"
msgstr ""

#: apps/lgroupmod.c:62 apps/lgroupmod.c:64 apps/lgroupmod.c:66
#: apps/lgroupmod.c:68 apps/lgroupmod.c:70 apps/lgroupmod.c:72
#: apps/lpasswd.c:52 apps/lpasswd.c:54 apps/luseradd.c:61 apps/luseradd.c:63
#: apps/luseradd.c:65 apps/luseradd.c:67 apps/luseradd.c:71 apps/luseradd.c:77
#: apps/luseradd.c:79 apps/luseradd.c:81 apps/luseradd.c:83 apps/luseradd.c:85
#: apps/luseradd.c:87 apps/luseradd.c:89 apps/luseradd.c:91 apps/lusermod.c:59
#: apps/lusermod.c:61 apps/lusermod.c:65 apps/lusermod.c:71 apps/lusermod.c:73
#: apps/lusermod.c:75 apps/lusermod.c:81 apps/lusermod.c:83 apps/lusermod.c:85
#: apps/lusermod.c:87 apps/lusermod.c:89 apps/lusermod.c:91
msgid "STRING"
msgstr ""

#: apps/lgroupmod.c:64 apps/luseradd.c:79
msgid "pre-hashed password for use with group"
msgstr ""

#: apps/lgroupmod.c:66
msgid "list of administrators to add"
msgstr ""

#: apps/lgroupmod.c:68
msgid "list of administrators to remove"
msgstr ""

#: apps/lgroupmod.c:70
msgid "list of group members to add"
msgstr ""

#: apps/lgroupmod.c:72
msgid "list of group members to remove"
msgstr ""

#: apps/lgroupmod.c:73
msgid "lock group"
msgstr ""

#: apps/lgroupmod.c:74
msgid "unlock group"
msgstr ""

#: apps/lgroupmod.c:129 apps/lusermod.c:168
#, c-format
msgid "Both -L and -U specified.\n"
msgstr "I -L i -U su navedeni.\n"

#: apps/lgroupmod.c:145 apps/lgroupmod.c:161
#, c-format
msgid "Failed to set password for group %s: %s\n"
msgstr "Neuspelo postavljanje lozinke za grupu %s: %s\n"

#: apps/lgroupmod.c:177
#, c-format
msgid "Group %s could not be locked: %s\n"
msgstr "Grupa %s ne može biti zaključana: %s\n"

#: apps/lgroupmod.c:193
#, c-format
msgid "Group %s could not be unlocked: %s\n"
msgstr "Grupa %s ne može biti otključana: %s\n"

#: apps/lgroupmod.c:276 apps/lgroupmod.c:298
#, c-format
msgid "Group %s could not be modified: %s\n"
msgstr "Grupa %s ne može biti izmenjena: %s\n"

#: apps/lid.c:42 apps/lid.c:74 apps/lid.c:191
#, c-format
msgid "Error looking up %s: %s\n"
msgstr "Greška pri potrazi za %s: %s\n"

#: apps/lid.c:118
msgid ""
"list members of a named group instead of the group memberships for the named "
"user"
msgstr ""

#: apps/lid.c:121
msgid "only list membership information by name, and not UID/GID"
msgstr ""

#: apps/lid.c:148
#, c-format
msgid "No group name specified, using %s.\n"
msgstr "Nije navedeno ime grupe, koristim %s.\n"

#: apps/lid.c:152
#, c-format
msgid "No group name specified, no name for gid %d.\n"
msgstr "Nije navedeno ime grupe, nema imena za gid %d.\n"

#: apps/lid.c:163
#, c-format
msgid "No user name specified, using %s.\n"
msgstr "Nije navedeno korisničko ime, koristim %s.\n"

#: apps/lid.c:195
#, c-format
msgid "%s does not exist\n"
msgstr "%s ne postoji\n"

#: apps/lnewusers.c:49
msgid "file with user information records"
msgstr ""

#: apps/lnewusers.c:49
msgid "PATH"
msgstr ""

#: apps/lnewusers.c:51
msgid "don't create home directories"
msgstr ""

#: apps/lnewusers.c:53
msgid "don't create mail spools"
msgstr ""

#: apps/lnewusers.c:65
msgid "[OPTION...]"
msgstr "[OPCIJA...]"

#: apps/lnewusers.c:90
#, c-format
msgid "Error opening `%s': %s.\n"
msgstr "Greška pri otvaranju „%s“: %s.\n"

#: apps/lnewusers.c:121
#, c-format
msgid "Error creating account for `%s': line improperly formatted.\n"
msgstr "Greška pri pravljenju naloga za „%s“: nepravilno oblikovana linija.\n"

#: apps/lnewusers.c:132 apps/luseradd.c:132 apps/lusermod.c:146
#, c-format
msgid "Invalid user ID %s\n"
msgstr "Neispravan korisnički ID %s\n"

#: apps/lnewusers.c:139
msgid "Refusing to create account with UID 0.\n"
msgstr "Odbijam da napravim nalog sa UID 0.\n"

#: apps/lnewusers.c:209
#, c-format
msgid "Error creating group for `%s' with GID %jd: %s\n"
msgstr "Greška pri pravljenju grupe „%s“ sa GID %jd: %s\n"

#: apps/lnewusers.c:249
#, c-format
msgid "Refusing to use dangerous home directory `%s' for %s by default\n"
msgstr "Odbijam da podrazumevano upotrebim opasan direktorijum „%s“ za %s\n"

#: apps/lnewusers.c:260
#, c-format
msgid "Error creating home directory for %s: %s\n"
msgstr "Greška pri pravljenju ličnog direktorijuma za %s: %s\n"

#: apps/lnewusers.c:273
#, c-format
msgid "Error creating mail spool for %s: %s\n"
msgstr ""

#: apps/lnewusers.c:288
#, c-format
msgid "Error setting initial password for %s: %s\n"
msgstr "Greška pri postavljanju lozinke za %s: %s\n"

#: apps/lnewusers.c:298
#, c-format
msgid "Error creating user account for %s: %s\n"
msgstr "Greška pri pravljenju korisničkog naloga za %s: %s\n"

#: apps/lpasswd.c:50
msgid "set group password instead of user password"
msgstr ""

#: apps/lpasswd.c:52
msgid "new plain password"
msgstr ""

#: apps/lpasswd.c:54
msgid "new crypted password"
msgstr ""

#: apps/lpasswd.c:56
msgid "read new plain password from given descriptor"
msgstr ""

#: apps/lpasswd.c:59
msgid "read new crypted password from given descriptor"
msgstr ""

#: apps/lpasswd.c:85
#, c-format
msgid "Changing password for %s.\n"
msgstr "Menjam lozinku za %s.\n"

#: apps/lpasswd.c:113
msgid "New password"
msgstr "Nova lozinka"

#: apps/lpasswd.c:116
msgid "New password (confirm)"
msgstr "Nova lozinka (potvrda)"

#: apps/lpasswd.c:130
#, c-format
msgid "Passwords do not match, try again.\n"
msgstr "Lozinke se ne slažu, pokušajte ponovo.\n"

#: apps/lpasswd.c:135
#, c-format
msgid "Password change canceled.\n"
msgstr "Promena lozinke je otkazana.\n"

#: apps/lpasswd.c:170 apps/lpasswd.c:188
#, c-format
msgid "Error reading from file descriptor %d.\n"
msgstr ""

#: apps/lpasswd.c:210 apps/luseradd.c:323 apps/luseradd.c:333
#, c-format
msgid "Error setting password for user %s: %s.\n"
msgstr "Greška pri postavljanju lozinke za korisnika %s: %s.\n"

#: apps/lpasswd.c:220
#, c-format
msgid "Error setting password for group %s: %s.\n"
msgstr "Greška pri postavljanju lozinke za grupu %s: %s.\n"

#: apps/lpasswd.c:229
#, c-format
msgid "Password changed.\n"
msgstr "Lozinka je promenjena.\n"

#: apps/luseradd.c:59
msgid "create a system user"
msgstr ""

#: apps/luseradd.c:61
msgid "GECOS information for new user"
msgstr ""

#: apps/luseradd.c:63
msgid "home directory for new user"
msgstr ""

#: apps/luseradd.c:65
msgid "directory with files for the new user"
msgstr ""

#: apps/luseradd.c:67
msgid "shell for new user"
msgstr ""

#: apps/luseradd.c:69
msgid "uid for new user"
msgstr ""

#: apps/luseradd.c:71
msgid "group for new user"
msgstr ""

#: apps/luseradd.c:73
msgid "don't create home directory for user"
msgstr ""

#: apps/luseradd.c:75
msgid "don't create group with same name as user"
msgstr ""

#: apps/luseradd.c:81
msgid "common name for new user"
msgstr ""

#: apps/luseradd.c:83
msgid "given name for new user"
msgstr ""

#: apps/luseradd.c:85
msgid "surname for new user"
msgstr ""

#: apps/luseradd.c:87
msgid "room number for new user"
msgstr ""

#: apps/luseradd.c:89
msgid "telephone number for new user"
msgstr ""

#: apps/luseradd.c:91
msgid "home telephone number for new user"
msgstr ""

#: apps/luseradd.c:194
#, c-format
msgid "Group %jd does not exist\n"
msgstr "Grupa %jd ne postoji\n"

#: apps/luseradd.c:213 apps/luseradd.c:230
#, c-format
msgid "Error creating group `%s': %s\n"
msgstr "Greška pri pravljenju grupe „%s“: %s\n"

#: apps/luseradd.c:270
#, c-format
msgid "Account creation failed: %s.\n"
msgstr "Neuspelo pravljenje naloga: %s.\n"

#: apps/luseradd.c:298
#, c-format
msgid "Error creating %s: %s.\n"
msgstr "Greška pri pravljenju %s: %s.\n"

#: apps/luseradd.c:310
#, c-format
msgid "Error creating mail spool: %s\n"
msgstr ""

#: apps/luserdel.c:49
msgid "don't remove the user's private group, if the user has one"
msgstr ""

#: apps/luserdel.c:52
msgid "remove the user's home directory"
msgstr ""

#: apps/luserdel.c:98
#, c-format
msgid "User %s could not be deleted: %s.\n"
msgstr "Korisnik %s ne može da bude obrisan: %s.\n"

#: apps/luserdel.c:117
#, c-format
msgid "%s did not have a gid number.\n"
msgstr "%s nije imala gid broj.\n"

#: apps/luserdel.c:124
#, c-format
msgid "No group with GID %jd exists, not removing.\n"
msgstr "Ne postoji grupa sa GID %jd, ne uklanjam.\n"

#: apps/luserdel.c:131
#, c-format
msgid "Group with GID %jd did not have a group name.\n"
msgstr "Grupa sa GID %jd nije imala ime grupe.\n"

#: apps/luserdel.c:138
#, c-format
msgid "Group %s could not be deleted: %s.\n"
msgstr "Grupa %s ne može da bude obrisana: %s.\n"

#: apps/luserdel.c:158
#, fuzzy, c-format
msgid "Error removing home directory: %s.\n"
msgstr "greška pri uklanjanju ličnog direktorijuma za korisnika"

#: apps/luserdel.c:171
#, c-format
msgid "Error removing mail spool: %s"
msgstr ""

#: apps/lusermod.c:59
msgid "GECOS information"
msgstr ""

#: apps/lusermod.c:61
msgid "home directory"
msgstr ""

#: apps/lusermod.c:63
msgid "move home directory contents"
msgstr ""

#: apps/lusermod.c:65
msgid "set shell for user"
msgstr ""

#: apps/lusermod.c:67
msgid "set UID for user"
msgstr ""

#: apps/lusermod.c:69
msgid "set primary GID for user"
msgstr ""

#: apps/lusermod.c:71
msgid "change login name for user"
msgstr ""

#: apps/lusermod.c:73
msgid "plaintext password for the user"
msgstr ""

#: apps/lusermod.c:75
msgid "pre-hashed password for the user"
msgstr ""

#: apps/lusermod.c:76
msgid "lock account"
msgstr ""

#: apps/lusermod.c:79
msgid "unlock account"
msgstr ""

#: apps/lusermod.c:81
msgid "set common name for user"
msgstr ""

#: apps/lusermod.c:83
msgid "set given name for user"
msgstr ""

#: apps/lusermod.c:85
msgid "set surname for user"
msgstr ""

#: apps/lusermod.c:87
msgid "set room number for user"
msgstr ""

#: apps/lusermod.c:89
msgid "set telephone number for user"
msgstr ""

#: apps/lusermod.c:91
msgid "set home telephone number for user"
msgstr ""

#: apps/lusermod.c:186 apps/lusermod.c:205
#, c-format
msgid "Failed to set password for user %s: %s.\n"
msgstr "Neuspelo postavljanje lozinke za korisnika %s: %s.\n"

#: apps/lusermod.c:221
#, c-format
msgid "User %s could not be locked: %s.\n"
msgstr "Korisnik %s ne može da bude zaključan: %s.\n"

#: apps/lusermod.c:235
#, c-format
msgid "User %s could not be unlocked: %s.\n"
msgstr "Korisnik %s ne može da bude otključan: %s.\n"

#: apps/lusermod.c:262
#, c-format
msgid "Warning: Group with ID %jd does not exist.\n"
msgstr "Upozorenje: Grupa sa ID-om %jd ne postoji\n"

#: apps/lusermod.c:305
#, c-format
msgid "User %s could not be modified: %s.\n"
msgstr "Korisnik %s ne može da bude izmenjen: %s.\n"

#: apps/lusermod.c:362
#, c-format
msgid "Group %s could not be modified: %s.\n"
msgstr "Grupa %s ne može da bude izmenjena: %s.\n"

#: apps/lusermod.c:385
#, c-format
msgid "No old home directory for %s.\n"
msgstr "Nema starog ličnog direktorijuma za %s.\n"

#: apps/lusermod.c:391
#, c-format
msgid "No new home directory for %s.\n"
msgstr "Nema novog ličnog direktorijuma za %s.\n"

#: apps/lusermod.c:398
#, c-format
msgid "Error moving %s to %s: %s.\n"
msgstr "Greška pri premeštanju %s u %s: %s.\n"

#: lib/config.c:128
#, c-format
msgid "could not open configuration file `%s': %s"
msgstr "ne mogu da otvorim datoteku podešavanja „%s“: %s"

#: lib/config.c:134
#, c-format
msgid "could not stat configuration file `%s': %s"
msgstr "ne mogu da izvršim stat nad datotekom podešavanja „%s“: %s"

#: lib/config.c:143
#, c-format
msgid "configuration file `%s' is too large"
msgstr "datoteka podešavanja „%s“ je prevelika"

#: lib/config.c:159
#, c-format
msgid "could not read configuration file `%s': %s"
msgstr "ne mogu da pročitam datoteku podešavanja „%s“: %s"

#: lib/error.c:62
msgid "success"
msgstr "uspeh"

#: lib/error.c:64
msgid "module disabled by configuration"
msgstr "modul isključen u podešavanjima"

#: lib/error.c:66
msgid "generic error"
msgstr "uopštena greška"

#: lib/error.c:68
msgid "not enough privileges"
msgstr "nema dovoljno povlastica"

#: lib/error.c:70
msgid "access denied"
msgstr "pristup odbijen"

#: lib/error.c:72
msgid "bad user/group name"
msgstr "loše ime korisnika/grupe"

#: lib/error.c:74
msgid "bad user/group id"
msgstr "loš id korisnika/grupe"

#: lib/error.c:76
msgid "user/group name in use"
msgstr "ime korisnika/grupe je u upotrebi"

#: lib/error.c:78
msgid "user/group id in use"
msgstr "id korisnika/grupe je u upotrebi"

#: lib/error.c:80
msgid "error manipulating terminal attributes"
msgstr "greška pri udešavanju svojstava terminala"

#: lib/error.c:82
msgid "error opening file"
msgstr "greška pri otvaranju datoteke"

#: lib/error.c:84
msgid "error locking file"
msgstr "greška pri zaključavanju datoteke"

#: lib/error.c:86
msgid "error statting file"
msgstr "greška pri izvršavanju stat nad datotekom"

#: lib/error.c:88
msgid "error reading file"
msgstr "greška pri čitanju datoteke"

#: lib/error.c:90
msgid "error writing to file"
msgstr "greška pri upisu u datoteku"

#: lib/error.c:92
msgid "data not found in file"
msgstr "podaci nisu nađeni u datoteci"

#: lib/error.c:94
msgid "internal initialization error"
msgstr "unutrašnja greška inicijalizacije"

#: lib/error.c:96
msgid "error loading module"
msgstr "greška pri učitavanju modula"

#: lib/error.c:98
msgid "error resolving symbol in module"
msgstr "greška pri razrešavanju simbola u modulu"

#: lib/error.c:100
msgid "library/module version mismatch"
msgstr "neslaganje verzije biblioteke/modula"

#: lib/error.c:102
msgid "unlocking would make the password field empty"
msgstr "otključavanje bi učinilo polje lozinke praznim"

#: lib/error.c:105
msgid "invalid attribute value"
msgstr ""

#: lib/error.c:107
msgid "invalid module combination"
msgstr ""

#: lib/error.c:109
msgid "user's home directory not owned by them"
msgstr ""

#: lib/error.c:115
msgid "unknown error"
msgstr "nepoznata greška"

#: lib/misc.c:240
msgid "invalid number"
msgstr "neispravan broj"

#: lib/misc.c:254
msgid "invalid ID"
msgstr "neispravan ID"

#: lib/modules.c:61
#, c-format
msgid "no initialization function %s in `%s'"
msgstr "nema funkcije inicijalizacije %s u „%s“"

#: lib/modules.c:79
#, c-format
msgid "module version mismatch in `%s'"
msgstr "neslaganje verzije modula u „%s“"

#: lib/modules.c:92
#, c-format
msgid "module `%s' does not define `%s'"
msgstr "modul „%s“ ne definiše „%s“"

#: lib/prompt.c:88
msgid "error reading terminal attributes"
msgstr "greška pri čitanju svojstava terminala"

#: lib/prompt.c:95 lib/prompt.c:107
msgid "error setting terminal attributes"
msgstr "greška pri postavljanju svojstava terminala"

#: lib/prompt.c:101
msgid "error reading from terminal"
msgstr "greška pri čitanju sa terminala"

#: lib/user.c:218
msgid "name is not set"
msgstr "ime nije postavljeno"

#: lib/user.c:223
msgid "name is too short"
msgstr "ime je prekratko"

#: lib/user.c:228
#, c-format
msgid "name is too long (%zu > %d)"
msgstr "ime je predugačko (%zu > %d)"

#: lib/user.c:235
msgid "name contains non-ASCII characters"
msgstr "ime sadrži ne-ASCII znakove"

#: lib/user.c:242
msgid "name contains control characters"
msgstr "ime sadrži kontrolne znakove"

#: lib/user.c:249
msgid "name contains whitespace"
msgstr "ime sadrži razmak"

#: lib/user.c:261
msgid "name starts with a hyphen"
msgstr "ime počinje crticom"

#: lib/user.c:272
#, c-format
msgid "name contains invalid char `%c'"
msgstr "ime sadrži neispravan znak „%c“"

#: lib/user.c:308 lib/user.c:360
#, c-format
msgid "user %s has no UID"
msgstr "korisnik %s nema UID"

#: lib/user.c:310
#, c-format
msgid "user %s not found"
msgstr ""

#: lib/user.c:333 lib/user.c:361
#, c-format
msgid "group %s has no GID"
msgstr "grupa %s nema GID"

#: lib/user.c:335
#, c-format
msgid "group %s not found"
msgstr ""

#: lib/user.c:355
#, c-format
msgid "user %jd has no name"
msgstr "korisnik %jd nema ime"

#: lib/user.c:356
#, c-format
msgid "group %jd has no name"
msgstr "grupa %jd nema ime"

#: lib/user.c:364
msgid "user has neither a name nor an UID"
msgstr "korisnik nema ni ime niti UID"

#: lib/user.c:365
msgid "group has neither a name nor a GID"
msgstr "grupa nema ni ime niti GID"

#: lib/user.c:1326
#, c-format
msgid "Refusing to use dangerous home directory `%s' by default"
msgstr "Odbijam da podrazumevano upotrebim opasan direktorijum „%s“"

#: lib/user.c:2326
#, c-format
msgid "Invalid default value of field %s: %s"
msgstr "Neispravna podrazumevana vrednost za polje %s: %s"

#: lib/util.c:350 modules/files.c:374
#, c-format
msgid "error locking file: %s"
msgstr "greška pri zaključavanju datoteke: %s"

#: lib/util.c:754
#, c-format
msgid "couldn't get default security context: %s"
msgstr "ne mogu da dobavim podrazumevani bezbednosni kontekst: %s"

#: lib/util.c:781 lib/util.c:807 lib/util.c:833
#, c-format
msgid "couldn't get security context of `%s': %s"
msgstr "ne mogu da dobavim bezbednosni kontekst za „%s“: %s"

#: lib/util.c:787 lib/util.c:813 lib/util.c:839 lib/util.c:882
#, c-format
msgid "couldn't set default security context to `%s': %s"
msgstr "ne mogu da postavim podrazumevani bezbednosni kontekst na „%s“: %s"

#: lib/util.c:862
#, fuzzy, c-format
#| msgid "couldn't get default security context: %s"
msgid "couldn't obtain selabel file context handle: %s"
msgstr "ne mogu da dobavim podrazumevani bezbednosni kontekst: %s"

#: lib/util.c:872
#, c-format
msgid "couldn't determine security context for `%s': %s"
msgstr "ne mogu da utvrdim bezbednosni kontekst za „%s“: %s"

#: modules/files.c:129 modules/files.c:692 modules/files.c:1735
#: modules/files.c:2070 modules/files.c:2080 modules/files.c:2162
#: modules/files.c:2173 modules/files.c:2239 modules/files.c:2251
#: modules/files.c:2341 modules/files.c:2350 modules/files.c:2405
#: modules/files.c:2414 modules/files.c:2509 modules/files.c:2518
#, c-format
msgid "couldn't open `%s': %s"
msgstr "ne mogu da otvorim „%s“: %s"

#: modules/files.c:137 modules/files.c:1116 modules/files.c:1323
#: modules/files.c:1479
#, c-format
msgid "couldn't stat `%s': %s"
msgstr "ne mogu da izvršim stat nad „%s“: %s"

#: modules/files.c:161
#, c-format
msgid "error creating `%s': %s"
msgstr "greška pri pravljenju „%s“: %s"

#: modules/files.c:169
#, c-format
msgid "Error changing owner of `%s': %s"
msgstr "Greška pri promeni vlasnika „%s“: %s"

#: modules/files.c:175
#, c-format
msgid "Error changing mode of `%s': %s"
msgstr ""

#: modules/files.c:191
#, c-format
msgid "Error reading `%s': %s"
msgstr "Greška pri čitanju „%s“: %s"

#: modules/files.c:206 modules/files.c:217 modules/files.c:305
#: modules/files.c:467
#, c-format
msgid "Error writing `%s': %s"
msgstr "Greška pri pisanju „%s“: %s"

#: modules/files.c:247 modules/files.c:1127 modules/files.c:1331
#: modules/files.c:1488
#, c-format
msgid "couldn't read from `%s': %s"
msgstr "ne mogu da čitam iz „%s“: %s"

#: modules/files.c:256
#, c-format
msgid "Invalid contents of lock `%s'"
msgstr ""

#: modules/files.c:261
#, c-format
msgid "The lock %s is held by process %ju"
msgstr ""

#: modules/files.c:269
#, fuzzy, c-format
msgid "Error removing stale lock `%s': %s"
msgstr "Greška pri premeštanju %s u %s: %s.\n"

#: modules/files.c:297
#, fuzzy, c-format
msgid "error opening temporary file for `%s': %s"
msgstr "Greška pri otvaranju „%s“: %s.\n"

#: modules/files.c:321
#, c-format
msgid "Cannot obtain lock `%s': %s"
msgstr ""

#: modules/files.c:434
#, fuzzy, c-format
msgid "Error resolving `%s': %s"
msgstr "Greška pri čitanju „%s“: %s"

#: modules/files.c:442
#, fuzzy, c-format
msgid "Error replacing `%s': %s"
msgstr "Greška pri čitanju „%s“: %s"

#: modules/files.c:920
#, fuzzy, c-format
#| msgid "entity object has no %s attribute"
msgid "duplicate object has no %s attribute"
msgstr "objekat entiteta nema %s svojstvo"

#: modules/files.c:930
#, fuzzy, c-format
#| msgid "object has no %s attribute"
msgid "original object has no %s attribute"
msgstr "objekat nema %s svojstvo"

#: modules/files.c:942
#, c-format
msgid "ID %lu already in use by %s"
msgstr ""

#: modules/files.c:1025
#, fuzzy, c-format
msgid "%s value `%s': `\\n' not allowed"
msgstr "%s vrednost „%s“: „:“ nije dozvoljena"

#: modules/files.c:1032
#, c-format
msgid "%s value `%s': `:' not allowed"
msgstr "%s vrednost „%s“: „:“ nije dozvoljena"

#: modules/files.c:1136
msgid "entry already present in file"
msgstr "stavka je već prisutna u datoteci"

#: modules/files.c:1143 modules/files.c:1153 modules/files.c:1163
#: modules/files.c:1543 modules/files.c:1551 modules/files.c:1559
#, c-format
msgid "couldn't write to `%s': %s"
msgstr "ne mogu da upišem u „%s“: %s"

#: modules/files.c:1309
#, c-format
msgid "entity object has no %s attribute"
msgstr "objekat entiteta nema %s svojstvo"

#: modules/files.c:1351
msgid "entry with conflicting name already present in file"
msgstr "stavka sa sukobljenim imenom je već prisutna u datoteci"

#: modules/files.c:1953
#, fuzzy
msgid "`:' and `\\n' not allowed in encrypted password"
msgstr "„:“ nije dozvoljena u šifrovanoj lozinci"

#: modules/files.c:1965 modules/ldap.c:1543 modules/ldap.c:1812
msgid "error encrypting password"
msgstr "greška pri šifrovanju lozinke"

#: modules/files.c:2667 modules/ldap.c:2410
#, c-format
msgid "the `%s' and `%s' modules can not be combined"
msgstr ""

#: modules/files.c:2751 modules/files.c:2829
msgid "not executing with superuser privileges"
msgstr "ne izvršavam sa administratorskim povlasticama"

#: modules/files.c:2842
msgid "no shadow file present -- disabling"
msgstr "nema prisutne shadow datoteke -- isključujem"

#: modules/ldap.c:199
msgid "error initializing ldap library"
msgstr "greška pri inicijalizaciji ldap biblioteke"

#: modules/ldap.c:210
#, c-format
msgid "could not set LDAP protocol to version %d"
msgstr "ne mogu da postavim LDAP protokol na verziju %d"

#: modules/ldap.c:229
msgid "could not negotiate TLS with LDAP server"
msgstr "nisam mogao da pregovaram TLS sa LDAP serverom"

#: modules/ldap.c:424
msgid "could not bind to LDAP server"
msgstr "ne mogu da se vežem za LDAP server"

#: modules/ldap.c:427
#, c-format
msgid "could not bind to LDAP server, first attempt as `%s': %s"
msgstr "ne mogu da se vežem za LDAP server, prvo pokušajte kao „%s“: %s"

#: modules/ldap.c:1315
#, c-format
msgid "user object had no %s attribute"
msgstr "korisnički objekat nema %s svojstvo"

#: modules/ldap.c:1324
#, c-format
msgid "user object was created with no `%s'"
msgstr "korisnički objekat je napravljen bez „%s“"

#: modules/ldap.c:1344
#, c-format
msgid "error creating a LDAP directory entry: %s"
msgstr "greška pri pravljenju stavke LDAP kataloga: %s"

#: modules/ldap.c:1370 modules/ldap.c:1604
#, c-format
msgid "error modifying LDAP directory entry: %s"
msgstr "greška pri izmeni stavke LDAP kataloga: %s"

#: modules/ldap.c:1395
#, c-format
msgid "error renaming LDAP directory entry: %s"
msgstr "greška pri preimenovanju stavke LDAP kataloga: %s"

#: modules/ldap.c:1440
#, c-format
msgid "object had no %s attribute"
msgstr "objekat nije imao %s svojstvo"

#: modules/ldap.c:1456
#, c-format
msgid "error removing LDAP directory entry: %s"
msgstr "greška pri uklanjanju stavke LDAP kataloga: %s"

#: modules/ldap.c:1506 modules/ldap.c:1521 modules/ldap.c:1635
#: modules/ldap.c:1730
#, c-format
msgid "object has no %s attribute"
msgstr "objekat nema %s svojstvo"

#: modules/ldap.c:1533
msgid "unsupported password encryption scheme"
msgstr "nepodržana šema šifrovanja lozinke"

#: modules/ldap.c:1658
msgid "no such object in LDAP directory"
msgstr "nema takvog objekta u LDAP katalogu"

#: modules/ldap.c:1670
#, c-format
msgid "no `%s' attribute found"
msgstr "nije pronađeno „%s“ svojstvo"

#: modules/ldap.c:1843
#, c-format
msgid "error setting password in LDAP directory for %s: %s"
msgstr "greška pri postavljanju lozinke u LDAP katalogu za %s: %s"

#: modules/ldap.c:2446
msgid "LDAP Server Name"
msgstr "Ime LDAP servera"

#: modules/ldap.c:2452
msgid "LDAP Search Base DN"
msgstr "Osnovni DN LDAP pretrage"

#: modules/ldap.c:2458
msgid "LDAP Bind DN"
msgstr "DN LDAP uvezivanja"

#: modules/ldap.c:2465
msgid "LDAP Bind Password"
msgstr "Lozinka LDAP uvezivanja"

#: modules/ldap.c:2471
msgid "LDAP SASL User"
msgstr "LDAP SASL korisnik"

#: modules/ldap.c:2478
msgid "LDAP SASL Authorization User"
msgstr "LDAP SASL ovlašćujući korisnik"

#: modules/sasldb.c:132
#, c-format
msgid "Cyrus SASL error creating user: %s"
msgstr "Cyrus SASL greška pri pravljenju korisnika: %s"

#: modules/sasldb.c:136
#, c-format
msgid "Cyrus SASL error removing user: %s"
msgstr "Cyrus SASL greška pri uklanjanju korisnika: %s"

#: modules/sasldb.c:503 modules/sasldb.c:511
#, c-format
msgid "error initializing Cyrus SASL: %s"
msgstr "greška pri inicijalizaciji Cyrus SASL-a: %s"

#: python/admin.c:505
msgid "error creating home directory for user"
msgstr "greška pri pravljenju ličnog direktorijuma za korisnika"

#: python/admin.c:544 python/admin.c:583
msgid "error removing home directory for user"
msgstr "greška pri uklanjanju ličnog direktorijuma za korisnika"

#: python/admin.c:654
msgid "error moving home directory for user"
msgstr "greška pri premeštanju ličnog direktorijuma za korisnika"

#: samples/lookup.c:63
#, c-format
msgid "Error initializing %s: %s\n"
msgstr "Greška pri inicijalizaciji %s: %s\n"

#: samples/lookup.c:76
#, c-format
msgid "Invalid ID %s\n"
msgstr "Neispravan ID %s\n"

#: samples/lookup.c:88
#, c-format
msgid "Searching for group with ID %jd.\n"
msgstr "Pretražujem za grupom sa ID %jd.\n"

#: samples/lookup.c:92
#, c-format
msgid "Searching for group named %s.\n"
msgstr "Pretražujem za grupom sa imenom %s.\n"

#: samples/lookup.c:99
#, c-format
msgid "Searching for user with ID %jd.\n"
msgstr "Pretražujem za korisnikom sa ID %jd.\n"

#: samples/lookup.c:103
#, c-format
msgid "Searching for user named %s.\n"
msgstr "Pretražujem za korisnikom sa imenom %s.\n"

#: samples/lookup.c:117
msgid "Entry not found.\n"
msgstr "Stavka nije pronađena.\n"

#: samples/prompt.c:48
msgid "Prompts succeeded.\n"
msgstr "Odzivi su uspeli.\n"

#: samples/prompt.c:58
msgid "Prompts failed.\n"
msgstr "Odzivi nisu uspeli.\n"

#: samples/testuser.c:76
msgid "Default user object classes:\n"
msgstr "Podrazumevane klase korisničkih objekata:\n"

#: samples/testuser.c:82
msgid "Default user attribute names:\n"
msgstr "Podrazumevana imena korisničkih svojstava:\n"

#: samples/testuser.c:88
msgid "Getting default user attributes:\n"
msgstr "Dobavljam podrazumevana korisnička svojstva:\n"

#: samples/testuser.c:95
msgid "Copying user structure:\n"
msgstr "Umnožavam korisničku strukturu:\n"

#~ msgid "backup file `%s' exists and is not a regular file"
#~ msgstr "rezervna datoteka „%s“ postoji i nije redovna datoteka"

#~ msgid "backup file size mismatch"
#~ msgstr "neslaganje veličine rezervne datoteke"
